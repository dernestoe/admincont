var http=createRequestObject();
var uploader="";
var uploadDir="";
var dirname="";
var filename="";
var timeInterval="";
var idname="";

function createRequestObject() {
    var obj;
    var browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer"){
    	return new ActiveXObject("Microsoft.XMLHTTP");
    }
    else{
    	return new XMLHttpRequest();
    }   
}
function traceUpload() {
   http.onreadystatechange = handleResponse;
   http.open("GET", 'salvar_encabezado.php?uploadDir='+uploadDir+'&dirname='+dirname+'&filename='+filename+'&uploader='+uploader); 
   http.send(null);   
}
function handleResponse() {
	if(http.readyState == 4){
		var response=http.responseText; 
		if(response.indexOf("ˇˇ") != -1){
			clearInterval(timeInterval);
			//document.getElementById('loading'+idname).innerHTML="";
		}
        document.getElementById(uploaderId).innerHTML=response;
		//document.getElementById('ValueFile').value	= ''
    }
    else {
    	document.getElementById(uploaderId).innerHTML=CargandoArchivo;
    }
}
function uploadFile(obj, dname, Bton) {
/*Inicializando variables*/
http=createRequestObject();
uploadDir=obj.value;
idname=obj.name;
dirname=dname;
			
if (navigator.appVersion.indexOf("Windows") !=-1) {
			 //alert("Tu sistema operativo es Windows");
			 Barra='\\'
		 }
		else {
			 //alert("Tu estas trabajando en Linux");
			 Barra='/';
			 }
filename=uploadDir.substr(uploadDir.lastIndexOf(Barra)+1);

document.getElementById('loading'+idname).innerHTML="";
uploaderId = 'uploader'+obj.name;
uploader = obj.name;
document.getElementById('formName'+obj.name).submit();
//setInterval se ejecutará una y otra vez en intervalos de x segundos,
timeInterval=setInterval("traceUpload()", 1500);
//alert(Bton);
document.getElementById(Bton).value 	= TextValueBoton;
document.getElementById(Bton).disabled  = true;
document.getElementById('Cargando').innerHTML	= "<img src='./icons/ajax-loader.gif'></img>";
}
	
function comprueba_extension(formulario) {
	
   extensiones_permitidas = new Array("banner.jpg");
   mierror = "";
   archivo = document.formNameid1.id1.value;
   if (!archivo) {
 
       mierror = ErrorNoSeleciono;
	   
   }else{
      
       if (navigator.appVersion.indexOf("Windows") !=-1) {
			 Barra='\\'
		 }
		else {
			  Barra='/';
			 }
	  Extension = (archivo.substring(archivo.lastIndexOf(Barra)+1)).toLowerCase();
	  
      permitida = false;
      for (var i = 0; i < extensiones_permitidas.length; i++) {
	  	 //alert(extension);
		
         if (extensiones_permitidas[i] == Extension) {
         permitida = true;
         break;
         }
      }
      if (!permitida) {
         mierror = ErrorNoExtension;
       }else{
          //submito!
         //alert ("Todo correcto. Voy a submitir el formulario.");
         formulario.submit();
         return 1;
       }
   }
   //si estoy aqui es que no se ha podido submitir
   alert (mierror);
   return false;
}