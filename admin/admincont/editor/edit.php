<?php 
  require_once('config.php');
  //we will get the record based on the ID wchih was passed in via the querystring
  //if the id == 0, the we are creating a new page.
  $id = $_REQUEST['id']; 


  $browserTitle = 'Create new Page';
  $content = '';
  $summary = '';
  $title = '';

  //We are editing a page, so let's get the values from the database
  if ($id){
    $dbh = getConn();
    //get the specified record
    $rs = mysql_query("SELECT * FROM page WHERE PageID = " . $id, $dbh) or die ('An error occured: ' . mysql_error());
    mysql_close($dbh);
    if (!mysql_num_rows($rs)){
      Header('Location: index.php');  //something weird happened, send them back to the list
      ob_flush();
    }
    $row = mysql_fetch_assoc($rs);
    $content = $row['Content'];
    $summary = $row['Summary'];
    $title = $row['Title'];
  }
?>
<html>
  <head>
    <title><?php =$browserTitle?></title>
  <script language="jscript">
  function onLoad(){
    var ewe = new EWE(null, null);
    ewe.load(document.all.eweContainer,document.all.loadContent.innerHTML);
  }
  function save(){
    if (ViewCurrent == 2)   toggleView();
    document.forms['sample'].content.value = cleanup(document.all.ewe.innerHTML);
    document.forms['sample'].submit();
  }
  function sampleSave(){
    //the save button of the editor calls a method called "sampleSave", but I just wrap it around the real save() function.
    save();
  }
  var editorPath = "editor/";
  </script>
  <script language="JScript" src="editor/source/ewe.js"></script>
  </head>
  <body bgcolor="#FFFFFF" color="000000" onLoad="onLoad();">
<!--LOOK HOW I PLACE THE $Content INSIDE THIS HIDDEN DIV TAG, and look at the onLoad function where I call ewe.load...//-->
  <div id="loadContent" style="position:absolute;visibility:hidden;height:1px;overflow:hidden;font-size:1px;"><?php =$content?></div>

  <h4><?php =$browserTitle?></h4>

  <form name="sample" method="post" action="save.php" onSubmit="save();">
    <b>Title</b> <input type="text" name="title" value="<?php =$title?>" size="20" maxlength="200"><br>
    <b>Summary</b><br>
    <textarea cols="40" rows="3" name="summary"><?php =$summary?></textarea><br><br>

    <b>Content</b>
    <div id="eweContainer"></div>
<!--LOOK HOW THIS IS A HIDDEN FIELD.  The save() function (called on form submit), copies the content of the editor into this!//-->
    <input type="hidden" name="content"><br>
    <input type="submit" value="Save">
    <input type="hidden" name="id" value="<?php =$id?>">
  </form>
  </body>
</html>