<?php 

ob_start('ob_gzhandler');         //always a good idea to turn this on.

DEFINE('DDB_HOST', 'localhost');  //the address of the database
DEFINE('DDB_USER', 'root');       //the username for the database
DEFINE('DDB_PASS', '');           //the password for the database
DEFINE('DDB_NAME', 'EXPRESS_UK');        //the name of the database


# Function header
# returns a database handler
function getConn(){
  $dbh = mysql_connect(DDB_HOST, DDB_USER, DDB_PASS);
  mysql_select_db(DDB_NAME, $dbh);
  return $dbh;
}