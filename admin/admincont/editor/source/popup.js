function popupToggle(popup, x, y){
  if (!prevRange){document.all.ewe.focus(); setRange();}   
  if (popup.style.visibility == "visible"){
    popup.style.visibility = "hidden";
    removeZindex(popup.style.zIndex);
  }else if (ViewCurrent == 1 || popup.enableInHTML == "true"){
    popup.style.zIndex = addZindex();
    popup.style.visibility = "visible";
    if (!popup.style.left){
      if (x && y){
        popup.style.left = x;
        popup.style.top = y;
      }else if (event){
        popup.style.left = event.x;
        popup.style.top = event.y;
      }
    }
  }
  if (event){
    event.cancelBubble = true;
    event.returnValue = false;
  }
}

function iframe_drag(img){
  if (event.button == 1){
    var c = img.parentElement;
    c.style.left = event.clientX - img.offsetLeft - 10;
    c.style.top = event.clientY - img.offsetTop - 10;
  }
}

function popupHide(popup){
  popup.style.visibility = "hidden";
  removeZindex(popup.style.zIndex);  
  if (event){
    event.cancelBubble = true;
    event.returnValue = false;
  }
}

function popupClick(popup){
  var maxZ = popupZindex[popupZindex.length-1];
  var currentZ = popup.style.zIndex;
  if (currentZ != maxZ){
    removeZindex(currentZ);
    popupZindex.push(++maxZ);
    popup.style.zIndex = maxZ;
  }
}

function removeZindex(current){
  var tmpArray = new Array();
  for (var i = 0; i < popupZindex.length; ++i){
    if (current != popupZindex[i]){
      tmpArray.push(popupZindex[i]);
    }
  }
  popupZindex = tmpArray;
}
function addZindex(){
  if (popupZindex.length == 0){
    popupZindex.push(1);
    return 1;
  }
  var max = popupZindex[popupZindex.length-1] + 1;
  popupZindex.push(max);
  return max;
} 

function showWindow(wd){
  if (!prevRange){document.all.ewe.focus(); setRange();} 
  var wn;
  switch (wd){
  case 'hline':
    wn = window.open(editorPath + 'popup/hline.html', 'hline', 'height=200,width=300,left=475,top=25,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,directories=no,status=no');
    break;
  }
  wn.focus();
}
function popupShowEditTD(){
  if (!td) return;
  document.frames["frmeditTD"].loadTD(td);
  contextToggle();
  popupToggle(document.all.editTD);
}

function popupShowHREF(){
  document.frames["frmhyperlink"].load();
  popupToggle(document.all.hyperlink);
}

function popupShowForeColor(loc, x, y){
  document.frames["frmforecolorPopup"].load(loc);
  if (x && y) popupToggle(document.all.forecolorPopup, x, y);
  else popupToggle(document.all.forecolorPopup, x, y);
}

function popupShowEditTABLE(){
  if (!table) return;
  document.frames["frmEditTable"].loadTable(table);
  contextToggle();
  popupToggle(document.all.EditTable);
}
function popupShowEditIMG(){
  if (!img) return;
  document.frames["frmeditImage"].loadIMG(img);
  contextToggle();
  popupToggle(document.all.editImage);  
}