-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.37-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para adcont
CREATE DATABASE IF NOT EXISTS `adcont` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `adcont`;

-- Volcando estructura para tabla adcont.adcnt_conf
CREATE TABLE IF NOT EXISTS `adcnt_conf` (
  `Dominio` varchar(255) NOT NULL DEFAULT '',
  `multilang` tinyint(1) NOT NULL DEFAULT '0',
  `cabezote` tinyint(1) NOT NULL DEFAULT '0',
  `marquezina` tinyint(1) NOT NULL DEFAULT '0',
  `banners` tinyint(1) NOT NULL DEFAULT '0',
  `uspriv` smallint(6) NOT NULL DEFAULT '0',
  `C_amigo` smallint(6) NOT NULL DEFAULT '0',
  `pre_pag` varchar(26) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.adcnt_conf: 1 rows
/*!40000 ALTER TABLE `adcnt_conf` DISABLE KEYS */;
INSERT INTO `adcnt_conf` (`Dominio`, `multilang`, `cabezote`, `marquezina`, `banners`, `uspriv`, `C_amigo`, `pre_pag`, `id`) VALUES
	('Febancol', 0, 0, 0, 0, 0, 0, 'home.php', 0);
/*!40000 ALTER TABLE `adcnt_conf` ENABLE KEYS */;

-- Volcando estructura para tabla adcont.distributors
CREATE TABLE IF NOT EXISTS `distributors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linea` varchar(200) NOT NULL DEFAULT '',
  `producto` varchar(244) NOT NULL DEFAULT '',
  `foto` varchar(88) NOT NULL DEFAULT '',
  `color` varchar(44) NOT NULL DEFAULT '',
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `transito` int(11) NOT NULL DEFAULT '0',
  `llegada` varchar(26) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.distributors: 0 rows
/*!40000 ALTER TABLE `distributors` DISABLE KEYS */;
/*!40000 ALTER TABLE `distributors` ENABLE KEYS */;

-- Volcando estructura para tabla adcont.files
CREATE TABLE IF NOT EXISTS `files` (
  `Nombre_File` varchar(200) NOT NULL DEFAULT '',
  `Titulo` varchar(200) NOT NULL DEFAULT '',
  `Descrip` varchar(255) NOT NULL DEFAULT '',
  `FecUpload` varchar(44) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.files: 0 rows
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
/*!40000 ALTER TABLE `files` ENABLE KEYS */;

-- Volcando estructura para tabla adcont.file_document
CREATE TABLE IF NOT EXISTS `file_document` (
  `id_doc` int(11) NOT NULL AUTO_INCREMENT,
  `archivo` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_doc`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.file_document: 0 rows
/*!40000 ALTER TABLE `file_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_document` ENABLE KEYS */;

-- Volcando estructura para tabla adcont.file_images
CREATE TABLE IF NOT EXISTS `file_images` (
  `id_image` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_image`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.file_images: 0 rows
/*!40000 ALTER TABLE `file_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_images` ENABLE KEYS */;

-- Volcando estructura para tabla adcont.site_web
CREATE TABLE IF NOT EXISTS `site_web` (
  `PageID` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_link` smallint(6) NOT NULL DEFAULT '0',
  `Title` varchar(26) NOT NULL DEFAULT '',
  `Idioma` char(2) NOT NULL DEFAULT '',
  `Content` longtext,
  `priv` int(11) DEFAULT NULL,
  PRIMARY KEY (`PageID`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.site_web: 25 rows
/*!40000 ALTER TABLE `site_web` DISABLE KEYS */;
INSERT INTO `site_web` (`PageID`, `tipo_link`, `Title`, `Idioma`, `Content`, `priv`) VALUES
	(1, 0, 'Home', 'es', '<h1>Home</h1>\r\n<p> EStamos probando el contenido de esta pagina</p>', NULL),
	(2, 0, 'Nosotros', 'es', '<h1>Nosotros</h1>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', NULL);
/*!40000 ALTER TABLE `site_web` ENABLE KEYS */;

-- Volcando estructura para tabla adcont.users
CREATE TABLE IF NOT EXISTS `users` (
  `Nombre` varchar(200) NOT NULL DEFAULT '',
  `Email` varchar(88) NOT NULL DEFAULT '',
  `Dir` varchar(250) NOT NULL DEFAULT '',
  `Tel` varchar(44) NOT NULL DEFAULT '',
  `Ciudad` varchar(88) NOT NULL DEFAULT '',
  `User` varchar(88) NOT NULL DEFAULT '',
  `Pass` varchar(88) NOT NULL DEFAULT '',
  `FecVinc` date NOT NULL DEFAULT '0000-00-00',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.users: 0 rows
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla adcont.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(244) NOT NULL DEFAULT '',
  `user` varchar(44) NOT NULL DEFAULT '',
  `pass` varchar(44) NOT NULL DEFAULT '',
  `activo` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla adcont.usuarios: 0 rows
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
